#!/bin/bash
# The line above must be formatted this way to run the script with Bash
# This won't run by default. In terminal, type this command without quotes:
# "chmod +x hello_world.sh" to make it executable. '-x' will remove execute
# privileges. Linux/Unix won't run it properly without doing this.

### INFORMATION ###
## This is a Bash Shell script. It is basically like running commands in the
## Linux/Unix terminal but automating them. This script will print colored text
## repeatedly with random colors. You can extend the colors used if you want and
## change the text and frequency of the random text color. Have questions? Go
## to https://gitlab.com/trivoxel/learn-code/issues and post your question. I
## will do my best to help you there.

### NOTES ###
## We will refer to the following chart for colors to change the 'text' color.
# Black        0;30     Dark Gray     1;30
# Red          0;31     Light Red     1;31
# Green        0;32     Light Green   1;32
# Brown/Orange 0;33     Yellow        1;33
# Blue         0;34     Light Blue    1;34
# Purple       0;35     Light Purple  1;35
# Cyan         0;36     Light Cyan    1;36
# Light Gray   0;37     White         1;37

### SECTION 1: Variables ###
## Variables are little values that can be plugged into your script. A variable
## makes coding more efficient because you can change something across a whole
## script instead of changing something you reuse over and over.
text="Hello, world!"    # Use quotes for strings of text
delay=1                 # Integers and floats don't need a quote
color="default"         # This is a default value that we'll overwrite later
## ANSII Terminal Colors ##
Red="\033[0;31m"
Orange="\033[0;33m"
Yellow="\033[1;33m"
Green="\033[0;32m"
Blue="\033[0;34m"
Purple="\033[0;35m"
None="\033[0m"          # This is the default terminal text color
# You can refer to the list in the 'NOTES' section if you want to extent this
# list. You will notice that there's a pattern. Use '\033[' inside the quote
# followed by the color code next to the names of the colors with a lowercase
# 'm' at the very end.

### SECTION 2: Functions ###
## Fuctions are similar to variables. A variable only stores a single value like
## a number or some text while a function stores a setion of code that will run
## over and over again.
.randomColor () { # Name your function here. The () is important.
  colorNumber=$((1 + RANDOM % 6)) # Generates a number between 1 and 6.
  # The stuff between the brackets is checking if it is true. The spaces between
  # the code and the brackets are important. It's important to note that having
  # no space before and after an equals sign means you set a variable. If you
  # have a space before and after the equals sign, it will test if it is equal
  # and report a 'true' value. If it reports 'true' it will run the code after
  # 'then'. If it is false it will go to the next section.
  if [ $colorNumber = 1 ]; then # 'if' starts the If statement.
    color=$Red # This sets 'color' the the value of Red from the first SECTION
  elif [ $colorNumber = 2 ]; then
  # Use '$' followed by the name of the variable to read the value.
    color=$Orange
  elif [ $colorNumber = 3 ]; then
    color=$Yellow
  elif [ $colorNumber = 4 ]; then
    color=$Green
  elif [ $colorNumber = 5 ]; then
    color=$Blue
  elif [ $colorNumber = 6 ]; then
    color=$Purple
  else # If all other checks return false, it will run this.
    color=$None
  fi # Close every if statement with 'fi' (backwards if)
}

.printColor () {
  .randomColor # We can call the '.randomColor' function from earlier
  echo -e $color$text $colorNumber
}

### SECTION 3: Running the code ###
# $1 is the first string of text after the script name. For example, in terminal
# you execute the script like this: './hello-world.sh' so $1 would be whatever
# you type after the script file. Ex. './hello-world.sh --once' means that $1
# in your script will be '--once'. Another example is if you type
# './hello-world.sh --once --some-text' $1 is '--once' and $2 is '--some-text'
if [[ $1 = "--once" ]]; then # Use double brackets for strings
  .printColor
  exit
else # If you don't type anything for $1 or it is misspelled, this will run
  while : # We want to run this over and over so we can loop it.
  do
    .printColor
    sleep $delay
  done
fi

### CHALLENGE ###
## I chalenge you to add Cyan, Magenta, and Light Blue to the colors used. Try
## changing the text to say something different too! Play around with flags
## ($1, $2, $3, etc.) and try to make it do more stuff.
